/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Arrays;

/**
 *
 * @author Administrator
 */
public class Transform {
    public static String transNumToCN(String text){
        String [] cnNum = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
        String [] cnCount = {"万","仟", "佰","拾"};
        String cnNumber = null;
        StringBuilder cnNumber1 = new  StringBuilder();
        int num [] = new int [7];
        double n = 0.00;
        int i;
        //将读取的字符串转成数字
        try{
        n = Double.parseDouble(text);
        }catch(NumberFormatException ee){cnNumber = "非法数字!"; return cnNumber;}
        //数字在0-99999范围内
        if( n>=0.00 && n<100000.00){
            //将数字的整数和小数部分拆开
            int m1 = (int) ((n*100)%100);
            int m2 = (int) Math.floor(n);
            //将整数和小数部分各个位置的数储存在数组num中
            num[0] = m2/10000;
            num[1] = (m2%10000)/1000;
            num[2] = (m2%1000)/100;
            num[3] = (m2%100)/10;
            num[4] = m2%10;
            num[5] = m1/10;
            num[6] = m1%10;
            }
        else if(n == 100000.00){
            cnNumber = "拾万元";
        }
        //数字不在0-10万这个范围内均为非法
        else {
            cnNumber = "超出转换范围!";
        }
            //寻找输入数字最开始的位置
            int index = 0;
            for(i = 0;i<7;i++) {
            	if(num[i]!=0) {
            		index = i;
            		break;
            	}
            }
            System.out.println(Arrays.toString(num));
            if(index == 0)
            {
            	for(i=0;i<=6;i++) {
            		if(num[i] != 0&&i<4)
            		 cnNumber1.append(cnNum[num[i]]+cnCount[i]);
            		if(num[i]==0&&i!=4)
            		 continue;
            		if(i==4) {
            			if(num[i]!=0)
            			 cnNumber1.append(cnNum[num[4]]+"元");
            			else
            				cnNumber1.append("元");
            		}
            		 
            		if(i==5)
            		 cnNumber1.append(cnNum[num[5]]+"角");
            		if(i==6)
            		 cnNumber1.append(cnNum[num[6]]+"分");		
            	}
            }
            	
            if(index == 1) 
            {
            	for(i=1;i<=6;i++) {
            		if(num[i] != 0&&i<4)
            		 cnNumber1.append(cnNum[num[i]]+cnCount[i]);
            		if(num[i]==0&&i!=4)
            		 continue;
            		if(i==4) {
            			if(num[i]!=0)
            			 cnNumber1.append(cnNum[num[4]]+"元");
            			else
            				cnNumber1.append("元");
            		}
            		 
            		if(i==5)
            		 cnNumber1.append(cnNum[num[5]]+"角");
            		if(i==6)
            		 cnNumber1.append(cnNum[num[6]]+"分");		
            	}
            }
            if(index == 2)
            {
            	for(i=2;i<=6;i++) {
            		if(num[i] != 0&&i<4)
            		 cnNumber1.append(cnNum[num[i]]+cnCount[i]);
            		if(num[i]==0&&i!=4)
            		 continue;
            		if(i==4) {
            			if(num[i]!=0)
            			 cnNumber1.append(cnNum[num[4]]+"元");
            			else
            				cnNumber1.append("元");
            		}
            		 
            		if(i==5)
            		 cnNumber1.append(cnNum[num[5]]+"角");
            		if(i==6)
            		 cnNumber1.append(cnNum[num[6]]+"分");		
            	}
            }
            if(index == 3)
            {
            	for(i=3;i<=6;i++) {
            		if(num[i] != 0&&i<4)
            		 cnNumber1.append(cnNum[num[i]]+cnCount[i]);
            		if(num[i]==0&&i!=4)
            		 continue;
            		if(i==4) {
            			if(num[i]!=0)
            			 cnNumber1.append(cnNum[num[4]]+"元");
            			else
            				cnNumber1.append("元");
            		}
            		 
            		if(i==5)
            		 cnNumber1.append(cnNum[num[5]]+"角");
            		if(i==6)
            		 cnNumber1.append(cnNum[num[6]]+"分");		
            	}
            }
            if(index == 4)
            {
            	for(i=4;i<=6;i++) {
            		if(num[i] != 0&&i<4)
            		 cnNumber1.append(cnNum[num[i]]+cnCount[i]);
            		if(num[i]==0&&i!=4)
            		 continue;
            		if(i==4) {
            			if(num[i]!=0)
            			 cnNumber1.append(cnNum[num[4]]+"元");
            			else
            				cnNumber1.append("元");
            		}
            		 
            		if(i==5)
            		 cnNumber1.append(cnNum[num[5]]+"角");
            		if(i==6)
            		 cnNumber1.append(cnNum[num[6]]+"分");		
            	}
            }
            if(index == 5)
            {
            	if(num[6]!=0)
              	  cnNumber1.append(cnNum[num[5]]+"角"+cnNum[num[6]]+"分");
            	else
            	  cnNumber1.append(cnNum[num[5]]+"角");
            }      	
            if(index == 6)
            	cnNumber = (cnNum[num[6]]+"分");
        return cnNumber1.toString();
    }  
}
