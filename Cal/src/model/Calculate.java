/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 *
 * @author 圣多美
 */
public class Calculate {
    public static  String calc(String expression){
    	int index= 0,i=0,flag=0;
    	Stack<String> op = new Stack<String>();
    	String[] Operator = {"+","-"," ","*","/"," ","(",")"};  //用于判断优先集的数组
    	ArrayList<String> digit = new ArrayList<String>();    //构建后缀表达式
    	for(i = 0;i<expression.length();i++)
    	{
    		
    		if(expression.charAt(i)=='+'||expression.charAt(i)=='-'||expression.charAt(i)=='*'||expression.charAt(i)=='/')
    		{
    			if(index<i)
    			  digit.add(expression.substring(index, i)); 
    		    index = i+1;
    		    if(op.isEmpty())
    		    {
    		    	op.push(expression.valueOf(expression.charAt(i)));  //字符串形式
    		    }
    		    else {   //判断优先集
    		    	int index1 = -1,index2 = -1;
    		    	for(int j = 0; j<Operator.length;j++)
    		    	{
    		    		if(op.peek().equals(Operator[j]))
    		    		{
    		    			if(j==6)             //遇到'('优先集更新
    		    				index1 = -2;
    		    			else
    		    			    index1 = j;
    		    		}		
    		    		if(expression.valueOf(expression.charAt(i)).equals(Operator[j]))
    		    			index2 = j;
    		    	}
    		    	if(index2-index1<2&&flag==0) //同优先集
    		    	{
    		    		digit.add(op.pop());
    		    		op.push(expression.valueOf(expression.charAt(i)));
    		    	}
    		    		
    		    	else if(index2-index1>=2&&flag==0){  //优先集大
    		    		op.push(expression.valueOf(expression.charAt(i)));
    		    	}
    		    	else if(index2-index1<2&&flag==1)   //带括号情况
    		    	{
    		    		digit.add(op.pop());
    		    		op.push(expression.valueOf(expression.charAt(i)));
    		    	}
    		    	else if(index2-index1>=2&&flag==1)
    		    	{
    		    		op.push(expression.valueOf(expression.charAt(i)));
    		    	}
    		    }
    		}
    		else if(expression.charAt(i)=='(')
    		{
    			flag=1;
    			index = i+1;
    			op.push("(");
    		}
    		else if(expression.charAt(i)==')')  
    		{
    			flag = 0;
    			if(index<i) {
    				digit.add(expression.substring(index, i)); 
    			}
    			
    			index = i+1;
    			while(op.peek()!="(")
    			{
    				digit.add(op.pop());  
    			}
    			op.pop();
    		}
    	}
    	if(index<i)
    	  digit.add(expression.substring(index, i)); //后缀构造完成
    	while(!op.isEmpty())
    	{
    		digit.add(op.pop());
    	}
    	Stack<String> operation = new Stack<String>();  //存放结果
    	System.out.println(digit.toString());
    	System.out.println(operation); 
    	for( i = 0;i<digit.size();i++)   //计算
    	{
    		double num1,num2;
    		String judge = digit.get(i);
    		if(digit.get(i).equals("+"))
    		{
    			num1 = Double.valueOf(operation.pop());
    			num2 = Double.valueOf(operation.pop());
    			operation.push(String.valueOf(num1+num2));
 
    		}
    		else if(digit.get(i).equals("-"))
    		{
    			num1 = Double.valueOf(operation.pop());
    			num2 = Double.valueOf(operation.pop());
    			operation.push(String.valueOf(num2-num1));
    		}
    		else if(digit.get(i).equals("*"))
    		{
    			num1 = Double.valueOf(operation.pop());
    			num2 = Double.valueOf(operation.pop());
    			operation.push(String.valueOf(num1*num2));
    		}
    		else if(digit.get(i).equals("/"))
    		{
    			num1 = Double.valueOf(operation.pop());
    			num2 = Double.valueOf(operation.pop());
    			operation.push(String.valueOf(num2/num1));
    		}
    		else {
    			operation.push(digit.get(i));
    		}
    	}
    	System.out.println(operation.peek());
    	return operation.pop();   //最后剩下的值输出
    }
}
